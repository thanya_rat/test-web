// Left Menu
const DashboardView = 10000000;
const ReportView = 20000000;
const TransactionView = 30000000;
const CouponView = 90000000;

// Report
const ReportSearch = 20000005;
const ReportExport = 20000006;
const TransactionSearch = 30000007;
const TransactionCancel = 30000008;

export default {
  DashboardView: DashboardView,
  ReportView: ReportView,
  ReportSearch: ReportSearch,
  ReportExport: ReportExport,
  TransactionView: TransactionView,
  TransactionSearch: TransactionSearch,
  TransactionCancel: TransactionCancel,
  CouponView: CouponView
}
