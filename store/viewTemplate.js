const {apiProject} = process.env
import * as _ from "lodash";

const state = () => ({
  loading: false,
  loaded: false,
  data: {},
})

const actions = {
  getViewTable({commit, rootState}) {
    commit('setLoading')
    return this.$axios.get(`${apiProject}/table`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
  // Table Field
  getTableData({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.get(`${apiProject}/table/${payload.table_id}?page=${payload.page}&q=${payload.textSearch}&date_from=${payload.date_from}&date_to=${payload.date_to}&sort_by_field=${payload.sort_by_field}&sort_by=${payload.sort_by}&root_id=${payload.root_id}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
  // Loop Create
  getTableField({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.get(`${apiProject}/table/${payload.table_id}/field`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      // let configData = _.groupBy(data.data, 'group_name')
      commit('setData', data.data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setData')
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
  // Loop Save Create
  addTableField({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.post(`${apiProject}/table/${payload.table_id}/field`,
      payload.parameter
      , {
        headers: {
          'Accept': 'application/json',
          'Authorization': `Bearer ${rootState.user.token}`
        }
      }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
  // Send Image Loop Data
  sendImageTableField({commit, rootState}, payload) {
    commit('setLoading')
    let formData = new FormData();
    formData.append('image', payload.image);
    return this.$axios.post(`${apiProject}/image`,formData , {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
  // Loop Load Edit
  getTableFieldEdit({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.get(`${apiProject}/table/${payload.table_id}/field/${payload.field_id}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      // let configData = _.groupBy(data.data, 'group_name')
      commit('setData', data.data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setData')
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },

  updateTableField({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.post(`${apiProject}/table/${payload.table_id}/field/${payload.field_id}`,
      payload.parameter
      , {
        headers: {
          'Accept': 'application/json',
          'Authorization': `Bearer ${rootState.user.token}`
        }
      }).then(({data}) => {
      // let configData = _.groupBy(data.data, 'group_name')
      commit('setData', data.data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setData')
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },

}

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },
  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },
  setData(state, data = []) {
    state.data = data
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
