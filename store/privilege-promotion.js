const {apiProject} = process.env

const state = {
  loading: false
};

const actions = {
  searchPrivilegePromotion({commit, rootState}, privilege_id){
    commit('setLoading')
    return this.$axios.$get(`${apiProject}/promotion-privilege/${privilege_id}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then((data) => {
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      return new Promise((resolve, reject) => {
        reject(error)
      });
    }).finally(() => {
      commit('setLoading', false)
    })
  }

};

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
