import Vuex from 'vuex'

import user from './user'
import report from './report'
import dashboard from './dashboard';
import viewTemplate from './viewTemplate';
//import sticker from './sticker';
//import materialPointSetting from './materialPointSetting';
//import assignSticker from './assignSticker';
import privilegePromotion from './privilege-promotion.js'
//import promotion from './promotion'
import banner from './banner';
import pointEarningPromotion from './pointEarningPromotion';
import redemption from './redemption';
import transaction from './transaction';
//import duplicateCode from './duplicate-code';
//import followUp from './follow-up';

const store = () => new Vuex.Store({
  modules: {
    user,
    //sticker,
    //materialPointSetting,
    // report,
    report,
    dashboard,
    viewTemplate,
    //assignSticker,
    privilegePromotion,
    //promotion,
    banner,
    pointEarningPromotion,
    redemption,
    transaction,
    //duplicateCode,
    //followUp
  }
})

export default store
