const {apiUrl} = process.env
const {apiLogin} = process.env

const state = () => ({
  userProfile: null,
  token: null,
  loading: false,
  loaded: false
})

const mutations = {
  setUser(state, user = null) {
    state.userProfile = user
  },
  setToken(state, token = null) {
    state.token = token
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },
  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },
  clearData(state) {
    state.loaded = false
    state.loading = false
    state.token = null
    state.userProfile = null
  }
}

const actions = {
  mount({commit, dispatch}) {
    if (localStorage.token) {
      commit('setToken', JSON.parse(localStorage.token))
      return dispatch('me').then((data)=>{
        return new Promise((resolve) => {
          resolve(data)
        })
      })
    }
    this.$router.replace('/login')
  },

  me({commit, state}) {
    commit('setLoading')

    if (!state.token) {
      if (localStorage.token) {
        commit('setToken', JSON.parse(localStorage.token))
      } else {
        commit('setLoading', false)
        return new Promise((resolve) => {
          resolve()
        })
      }
    }

    return this.$axios.get(`${apiUrl}/me`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${state.token}`
      }
    })
      .then(({data}) => {
        commit('setUser', data.data)
        commit('setLoaded')
        return new Promise((resolve) => {
          resolve(data.data)
        })
      })
      .catch((error) => {
        commit('setUser')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  login({commit}, payload) {
    commit('setLoading')
    return this.$axios.post(`${apiLogin}`, {
      grant_type: 'password',
      client_id: 2,
      client_secret: 'HVA7cVea4IdSMYFo9F4LcAWsIS3uK9vGPVIQVVWS',
      username: payload.username,
      password: payload.password
    }, {
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(({data}) => {
        commit('setToken', data.access_token)
        localStorage.setItem('token', JSON.stringify(data.access_token))
        commit('setLoaded')
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        commit('setUser')
        commit('setToken')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  logout({commit}) {
    commit('clearData')
    localStorage.removeItem('token')
  },

  changePassword({commit, state}, payload){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/password`, {
      current_password: payload.current_password,
      password: payload.new_password
    }, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${state.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
