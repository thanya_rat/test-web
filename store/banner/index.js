const {apiProject} = process.env

const state = {
  loading: false,
  loaded: false,
  data: [],
  meta: {},
  nextPage: null,
  previousPage: null,
  firstPage: null,
  lastPage: null
}

const actions = {
  getBanner({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$get(`${apiProject}/banners?page=${payload.page}&all=true`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setData', data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  createBanner({commit, rootState}, payload) {
    commit('setLoading')
    
    let formData = new FormData();
    formData.append('priority', payload.priority);
    formData.append('is_active', payload.is_active);
    formData.append('url', payload.url);
    formData.append('file', payload.file);
    formData.append('start_date', payload.start_date);
    formData.append('end_date', payload.end_date);

    return this.$axios.$post(`${apiProject}/banners`,formData, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  deleteBanner({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$delete(`${apiProject}/banners/${payload.id}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  checkActiveBanner({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$patch(`${apiProject}/banners/${payload.id}/active`, {},{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
}

const mutations = {
  setMeta(state, meta = {}) {
    state.meta = meta
  },
  setLinks(state, links) {
    state.nextPage = links.next
    state.previousPage = links.prev
    state.firstPage = links.first
    state.lastPage = links.last
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },

  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },

  setData(state, data = []) {
    state.data = data
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
