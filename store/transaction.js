const {apiProject} = process.env

const state = () => ({
  loading: false
})

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  getTransaction({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.get(`${apiProject}/transaction-point?date_from=${payload.date_from}&date_to=${payload.date_to}&search=${payload.search}&page=${payload.page}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
