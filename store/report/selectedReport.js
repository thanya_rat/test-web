const {apiUrl} = process.env

const state = {
  parameter: {},
  data: [],
  summary: {},
  pages: {},
  columns: {},
  exportData: {},
  loading: false,
  loaded: false
}
const actions = {
  selectedReport({commit, rootState}, payload) {
    commit('setLoading')

    this.$axios.get(`${apiUrl}/report/${payload}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        commit('setParameter', data.data)
      })
      .catch((error) => {
        commit('setParameter')
      })
      .finally(() => {
        commit('setLoading', false)
        commit('setLoaded', false)
      })
  },
  searchData({commit, rootState}, payload) {
    commit('setLoading')

    let urlParam = new URLSearchParams();

    _.forEach(payload, (value, key) => {
      urlParam.append(key, value)
    })

    return this.$axios.$post(`${apiUrl}/report/${payload.report_id}/detail`, urlParam, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data, summary, pages, columns}) => {

        columns = (_.isEmpty(columns) ? [] : JSON.parse(columns))
        summary = (_.isEmpty(summary) ? [] : JSON.parse(summary))

        commit('setData', data)
        commit('setSummary', summary)
        commit('setPage', pages)
        commit('setColumn', columns)
        commit('setLoaded')
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        commit('setData')
        commit('setSummary')
        commit('setPage')
        commit('setColumn')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
    // commit('setLoaded')
  },
  exportData({commit, rootState}, payload) {
    commit('setLoading')

    let urlParam = new URLSearchParams();

    _.forEach(payload, (value, key) => {
      urlParam.append(key, value)
    })

    return this.$axios.$post(`${apiUrl}/report/${payload.report_id}/detail`, urlParam, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data, columns, summary, report_header, report_summary}) => {

        data = (_.isEmpty(data) ? [] : JSON.parse(data))
        columns = (_.isEmpty(columns) ? [] : JSON.parse(columns))
        summary = (_.isEmpty(summary) ? [] : JSON.parse(summary))

        commit('setExportData', {
          rows: data,
          columns: columns,
          summary: summary,
          header: report_header,
          extension: report_summary
        })

        return new Promise((resolve) => {
          resolve({
            rows: data,
            columns: columns,
            summary: summary,
            header: report_header,
            extension: report_summary
          })
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  clearData({commit}){
    commit('setData')
    commit('setSummary')
    commit('setPage')
    commit('setColumn')
    commit('setLoaded', false)
    commit('setExportData')
  }
}
const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },

  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },

  setParameter(state, parameter = {}) {
    state.parameter = parameter
  },

  setData(state, data = []) {
    state.data = data
  },

  setExportData(state, data = {}){
    state.exportData = data
  },

  setSummary(state, summary = {}) {
    state.summary = summary
  },

  setPage(state, page = {}) {
    state.pages = page
  },

  setColumn(state, column = {}) {
    state.columns = column
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
