const {apiUrl} = process.env

import selectedReport from "./selectedReport";

const state = {
  loading: false,
  loaded: false,
  reports: []
};

const actions = {
  // mount({ commit, dispatch }) {
  //   if (localStorage.token) {
  //     commit('setToken', JSON.parse(localStorage.token))
  //     dispatch('me')
  //   }
  // },

  reports({commit, rootState}) {
    commit('setLoading')
    this.$axios.$get(`${apiUrl}/report`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then((data) => {
        commit('setReport', data.all_report)
        commit('setLoaded')
      })
      .catch((error) => {
        commit('setReport')
        commit('setLoaded', false)
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },

  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },

  setReport(state, reports = []) {
    state.reports = reports
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  modules:{
    selectedReport,
  }
}
