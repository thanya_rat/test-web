const {API_PROJECT} = process.env
const {apiUrlV2} = process.env

const state = {
  loading: false,
  loaded: false,
};

const actions = {
  loadDashBoardTemplate({commit, rootState}, payload) {
    commit('setLoading')

    return this.$axios.$get(`${apiUrlV2}/dashboard?dateFrom=${payload.dateFrom}&dateTo=${payload.dateTo}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then((data) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      });
    }).finally(() => {
      commit('setLoading', false)
    })
  },

};

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },
  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
