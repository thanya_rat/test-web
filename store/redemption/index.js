const {apiProject} = process.env
//const {apiUrlV2} = process.env

const state = {
  loading: false,
  loaded: false,
  data: [],
  meta: {},
  nextPage: null,
  previousPage: null,
  firstPage: null,
  lastPage: null
}

const actions = {
  getRedemption({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$get(`${apiProject}/deliveries?page=${payload.page}&q=${payload.text}&date_to=${payload.date_to}&date_from=${payload.date_from}&status=${payload.status}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data, links, meta}) => {
      commit('setData', data)
      commit('setLinks', links)
      commit('setMeta', meta)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  getRedemptionExport({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$get(`${apiProject}/deliveries?page=${payload.page}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setData', data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  updateRedemption({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$patch(`${apiProject}/deliveries/${payload.redemption_id}`,{}, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setData')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  addTrackingNumber({commit, rootState}, payload) {
    commit('setLoading')
    return this.$axios.$patch(`${apiProject}/deliveries/tracking/${payload.redemption_id}`, {
      tracking_no: payload.tracking_no
    }, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

}

const mutations = {
  setMeta(state, meta = {}) {
    state.meta = meta
  },
  setLinks(state, links) {
    state.nextPage = links.next
    state.previousPage = links.prev
    state.firstPage = links.first
    state.lastPage = links.last
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },

  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },

  setData(state, data = []) {
    state.data = data
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  // modules:{
  //   promotionCredit,
  // }

}
