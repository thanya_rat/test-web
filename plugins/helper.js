import Vue from 'vue'
const moment = require('moment')
import {mapState} from 'vuex'

Vue.mixin({
  computed: {
    ...mapState({
      userProfile: state => state.user.userProfile,
    })
  },
  methods:{
    isAccessPage(code){
      return true

      if(this.userProfile && code){
        let permission = _.find(this.userProfile.permission.access_module, (module)=>{
          return module.code === code.toString()
        })

        if(permission){
          return true
        }
      }
      return false
    },
    isAccessFunction(code){
      return true

      if(this.userProfile && code){
        let permission = _.find(this.userProfile.permission.access_object, (object)=>{
          return object.code === code.toString()
        })

        if(permission){
          return true
        }
      }
      return false
    },
    handleCatch(error){
      if (error.response) {
        let title = ''
        let message = ''

        if(error.response.data){
          if(error.response.data.messages){
            message = error.response.data.messages.error
          }else if(error.response.data.message){
            message = error.response.data.message
          }else{
            title = error.response.statusText + ' ('+error.response.status+')'
            message = error.response.data.exception
          }
        }else{
          title = error.response.statusText + ' ('+error.response.status+')'
          message = error.response.data.exception
        }

        if(error.response.status === 406){
          this.$router.replace('/login')
          return
        }

        $.alert({
          title: title,
          content: message
        })
      }
    },
    handleCatchLogin(error){
      if (error.response) {
        let title = ''
        let message = ''

        if(error.response.data){
          if(error.response.data.messages){
            message = error.response.data.messages.error
          }else if(error.response.data.message){
            message = error.response.data.message
          }else{
            title = error.response.statusText + ' ('+error.response.status+')'
            message = error.response.data.exception
          }
        }else{
          title = error.response.statusText + ' ('+error.response.status+')'
          message = error.response.data.exception
        }

        $.alert({
          title: title,
          content: message
        })
      }
    },
    formatPrice(value) {
      if(value){
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    },
    makeQueryString(data){
      let queryString = []

      _.each(data, (value, key) => {
        queryString.push(key + '=' + value)
      })

      queryString = _.join(queryString, '&')

      return queryString
    },
    transactionType(type, transactionType){
      if(transactionType === 'voucher'){
        return 'ใช้คูปอง'
      }
      if(transactionType === 'point'){
        if(type === '0'){
          return 'ใช้คะแนน'
        }else{
          return 'รับคะแนน'
        }
      }
      if(transactionType === 'credit'){
        if(type === '0'){
          return 'ใช้เครดิต'
        }else{
          return 'เติมเครดิต'
        }
      }
    }
  },
  filters: {
    moment(value, format) {
      if(value){
        return moment(value).format(format)
      }
      return ''
    },
    limitString(value, max = 200, format = ' ...') {
      if (value !== null || value !== undefined) {
        if (value.length > max) {
          return value.substr(0, max - 1) + format
        }
      }
      return value
    },
    formatPrice(value) {
      if(value){
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    },
    formatPriceAndTextVoucher(value) {
      if(value){
        if(value === 'voucher'){
          return 'voucher'
        }
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    }
  }
})
